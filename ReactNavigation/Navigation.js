import react from "react";
import { TabNavigationState } from "@react-navigation/native";
import NavigationContainer from "@react-navigation/native";

import HomeScreen from "./screens/HomeScreen";
import SettingsScreen from "./screens/SettingsScreen";
import StackScreen from "./screens/StackScreen";

const Tab = createBottomTabNavigator();

function myTabs() {
    return(
        <Tab.navigator
            initialRouteName="Home"
        >
            <Tab.Screen name="home" component={HomeScreen} />
            <Tab.Screen name="Settings" component={SettingsScreen} />
        </Tab.navigator>
    );
}

export default function Navigation() {
    return(
        <NavigationContainer>
            <myTabs />
        </NavigationContainer>
    );
}